package generatorrekordow.models;

import generatorrekordow.OracleConnection;
import java.util.Random;

public class Return {
    OracleConnection connection= new OracleConnection();
    Random random = new Random();
    int delivery;
    int status;
    int adress;

    public Return(int deliveryId, int adressId) {
        this.delivery= deliveryId;
        this.status=randomStatus();
        this.adress=adressId;
    }

    public int getDelivery() {
        return delivery;
    }

    public int getStatus() {
        return status;
    }

    public int getAdress() {
        return adress;
    }

    @Override
    public String toString() {
        return "Return{" + "delivery=" + delivery + ", status=" + status + ", adress=" + adress + '}';
    }

    private int randomStatus() {
        return random.nextInt(3) + 1;
    }
}
