package generatorrekordow.models;

import java.util.Random;
import generatorrekordow.OracleConnection;

public class Complaint {
    OracleConnection connection = new OracleConnection();
    String description;
    int status;
    int userId;
    Random random = new Random();
    
    public Complaint(int userId) {
        this.description=randomDescription();
        this.status=randomStatus();
        this.userId=userId;
    }

    public String getDescription() {
        return description;
    }

    public int getStatus() {
        return status;
    }

    public int getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return "Complaint{" + "description=" + description + ", status=" + status + ", userId=" + userId + '}';
    }
    private int randomStatus(){
        return random.nextInt(3)+1;
    }
    private String randomDescription(){
        return "Opis nr: "+random.nextInt(999);
    }
    
}
