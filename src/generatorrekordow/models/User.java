package generatorrekordow.models;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Random;

public class User {
    Random random = new Random();
    private String name;
    private String surname;
    private String mail;
    private String phone;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getMail() {
        return mail;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public String toString() {
        return "User{" + "name=" + name + ", surname=" + surname + ", mail=" + mail + ", phone=" + phone + '}';
    }

    
    
    public User() {
        this.name = randomName();
        this.surname = randomSurname();
        this.mail=randomMail(name, surname);
        this.phone=randomPhone();
    }
    private String randomName(){
        return potentialName[random.nextInt(potentialName.length)];
    }
    
    private String randomSurname(){
        return potentialSurname[random.nextInt(potentialSurname.length)];
    }
    private String randomMail(String name, String surname){
        return name+"."+surname+random.nextInt(900)+"@gmail.com";
    }
    private String randomPhone(){
        int number =random.nextInt(999999999)+100000000;
        return "+48"+number;
    }
    
   private String [] potentialName= {
     "Marcin","Adam","Monika","Natalia","Krzysztof","Pawel","Oskar","Michal","Kamil","Aleksandra","Sandra","Wanda","Sebastaian"  
   };
   private String [] potentialSurname= {
     "Klos","Wdowiak","Nowak","Sep","Pawlik","Michalak","Ogrodowczyk","Szpak","Skowronek","Zak","Sosna","Wicher","Majchrzak"  
   };
}
