package generatorrekordow.models;

import generatorrekordow.OracleConnection;
import java.util.Random;

public class Delivery {
    OracleConnection connection = new OracleConnection();
    Random random = new Random();
    int deliveryStatus;
    int deliveryAdress;

    public Delivery(int deliveryAdress) {
        this.deliveryAdress=deliveryAdress;
        this.deliveryStatus=randomStatus();
    }

    public int getDeliveryStatus() {
        return deliveryStatus;
    }

    public int getDeliveryAdress() {
        return deliveryAdress;
    }

    @Override
    public String toString() {
        return "Delivery{" + "deliveryStatus=" + deliveryStatus + ", deliveryAdress=" + deliveryAdress + '}';
    }
    
    private int randomStatus(){
        return random.nextInt(3)+1;
    }
}
