package generatorrekordow.models;

import generatorrekordow.OracleConnection;
import java.util.Random;

public class Order {
    OracleConnection connection= new OracleConnection();
    Random random = new Random();
    int userId;
    int productId;
    int statusId;

    public Order(int userId,int productId) {
        this.userId=userId;
        this.productId=productId;
        this.statusId=randomStatus();
    }

    @Override
    public String toString() {
        return "Order{userId=" + userId + ", productId=" + productId + ", statusId=" + statusId + '}';
    }
    

    public int getUserId() {
        return userId;
    }

    public int getProductId() {
        return productId;
    }

    public int getStatusId() {
        return statusId;
    }
    
    private int randomStatus(){
        return random.nextInt(3)+1;
    }
}
