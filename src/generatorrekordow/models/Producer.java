package generatorrekordow.models;

import static java.lang.Math.random;
import java.util.Random;

public class Producer {

    String name;
    String mail;
    String phone;
    String website;
    Random random=new Random();

    public Producer() {
        this.name=randomName();
        this.mail=name+"@gmail.com";
        this.website="www."+name+".com";
        this.phone= randomPhone();
    }

    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }

    public String getPhone() {
        return phone;
    }

    public String getWebsite() {
        return website;
    }

    
    @Override
    public String toString() {
        return "Producer{" + "name=" + name + ", mail=" + mail + ", phone=" + phone + ", website=" + website + ", random=" + random + ", potentialFirstPartName=" + potentialFirstPartName + ", potentialSecPartName=" + potentialSecPartName + '}';
    }

    private String randomName(){
        return potentialFirstPartName[random.nextInt(potentialFirstPartName.length)]+potentialSecPartName[random.nextInt(potentialSecPartName.length)];
    }

    private String randomPhone() {
        int number = random.nextInt(999999999) + 100000000;
        return "+48" + number;
    }
    private String[] potentialFirstPartName = {
        "Super", "Hiper", "Mega", "Nowa", "HiT", "best", "theBest", "good", "fit"
    };

    private String[] potentialSecPartName = {
        "Pilka", "PilkaNozna", "Footbal", "Fut", "Basketball", "Koszykowka", "Kosz", "Siatka", "Siatkowka", "Narty", "Ski"
    };
}
