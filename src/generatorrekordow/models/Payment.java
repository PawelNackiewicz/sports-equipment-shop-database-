package generatorrekordow.models;

import java.util.Random;

public class Payment {

    String currency = "PLN";
    int status;
    int price;
    Random random = new Random();

    public Payment() {
        this.price = randomPrice();
        this.status= randomStatus(); 
   }

    public String getCurrency() {
        return currency;
    }

    public int getStatus() {
        return status;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Payment{" + "currency=" + currency + ", status=" + status + ", price=" + price + '}';
    }

    private int randomPrice() {
        return random.nextInt(999);
    }

    private int randomStatus() {
        return random.nextInt(3) + 1;
    }
}
