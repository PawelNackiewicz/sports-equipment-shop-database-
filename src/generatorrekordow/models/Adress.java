package generatorrekordow.models;

import java.util.Random;

public class Adress {
    Random random = new Random();
    String city;
    String street;
    int number;

    public Adress() {
        this.city=randomCity();
        this.street=randomStreet();
        this.number=randomNumber();
    }

    @Override
    public String toString() {
        return "Adress{" + "city=" + city + ", street=" + street + ", number=" + number + '}';
    }
    

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public int getNumber() {
        return number;
    }
    
    String [] potentialCity={
        "Krakow","Opole","Warszawa","Ozimek","Szczedrzyk","Dzialoszyn","Poznan","Łeba","Szczecin","Gdańsk","Białystok"
    };
    
    String [] potentialStreet={
      "Kwiatowa","Sloneczna","Mokra","Budowlana","Wisniowa","Duza","Dluga","Krotka","Mala","Sniezna","Brzozowa","Fiolkowa","Marszalkowska"  
    };
    
    private String randomCity(){
        return potentialCity[random.nextInt(potentialCity.length)];
    }
    
    private String randomStreet(){
        return potentialStreet[random.nextInt(potentialStreet.length)];
    }
    private int randomNumber(){
        return random.nextInt(100)+1;
    }
    
}
