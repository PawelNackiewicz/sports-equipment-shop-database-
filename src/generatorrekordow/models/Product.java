package generatorrekordow.models;

import generatorrekordow.*;
import java.util.Random;

public class Product {

    MainFrame frame = new MainFrame();
    OracleConnection connection = frame.getConnection();
    Random random = new Random();
    String name;
    int price;
    int producerId;

    public Product(int producerId) {
        this.name = randomName();
        this.price = randomPrice();
        this.producerId = producerId;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getProducerId() {
        return producerId;
    }

    private String randomName() {
        return potentialName[random.nextInt(potentialName.length)] + " model " + random.nextInt(999);
    }

    private int randomPrice() {
        return random.nextInt(999);
    }

    private String[] potentialName = {
        "Buty ", "Narty ", "Pilka ", "Czapka", "Korki", "Koszulka"
    };

}
