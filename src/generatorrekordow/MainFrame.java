package generatorrekordow;

import generatorrekordow.models.*;
import java.awt.Color;
import java.sql.SQLException;
import java.util.Random;
import javax.swing.JLabel;

/**
 *
 * @author Pawel
 */
public class MainFrame extends javax.swing.JFrame {

    OracleConnection connection = new OracleConnection();

    public OracleConnection getConnection() {
        return connection;
    }

    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        initComponents();
        generateRecordsButton.setVisible(false);
        endLabel.setForeground(Color.RED);
        endLabel.setVisible(false);
    }

    public void setStstusLabel(JLabel StstusLabel) {
        this.StstusLabel = StstusLabel;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        UserLabel = new javax.swing.JLabel();
        PasswordLabel = new javax.swing.JLabel();
        UserFiled = new javax.swing.JTextField();
        StstusLabel = new javax.swing.JLabel();
        ConncetButton = new javax.swing.JButton();
        PortLabel = new javax.swing.JLabel();
        SIDLabel = new javax.swing.JLabel();
        HostLabel = new javax.swing.JLabel();
        HostFiled = new javax.swing.JTextField();
        SIDField = new javax.swing.JTextField();
        PortField = new javax.swing.JTextField();
        PasswordField = new javax.swing.JPasswordField();
        generateRecordsButton = new javax.swing.JButton();
        endLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("W2L4_ZAD2_SYMULATOR_Nackiewicz");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        UserLabel.setText("Nazwa użytkownika");

        PasswordLabel.setText("Hasło");

        UserFiled.setText("s91305");

        StstusLabel.setText("Brak Połączenia");

        ConncetButton.setText("Połącz");
        ConncetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConncetButtonActionPerformed(evt);
            }
        });

        PortLabel.setText("Port");

        SIDLabel.setText("SID");

        HostLabel.setText("Host");

        HostFiled.setText("217.173.198.135");

        SIDField.setText("orcltp");

        PortField.setText("1522");

        generateRecordsButton.setText("Generuj rekordy do bazy ");
        generateRecordsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateRecordsButtonActionPerformed(evt);
            }
        });

        endLabel.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        endLabel.setText("Gratulacje! Właśnie zostało wygenerowane 10 nowych rekordów do bazy dancyh!");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(HostLabel)
                        .addGap(18, 18, 18)
                        .addComponent(HostFiled, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(SIDLabel)
                        .addGap(18, 18, 18)
                        .addComponent(SIDField, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(PortLabel)
                        .addGap(18, 18, 18)
                        .addComponent(PortField, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(UserLabel)
                                .addGap(18, 18, 18)
                                .addComponent(UserFiled, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(PasswordLabel)
                                .addGap(18, 18, 18)
                                .addComponent(PasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ConncetButton, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(97, 97, 97)
                                .addComponent(StstusLabel)))
                        .addGap(104, 225, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(generateRecordsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(203, 203, 203))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(endLabel)
                        .addGap(38, 38, 38))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(UserLabel)
                    .addComponent(PasswordLabel)
                    .addComponent(UserFiled, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(HostLabel)
                    .addComponent(HostFiled, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SIDLabel)
                    .addComponent(SIDField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(PortLabel)
                    .addComponent(PortField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ConncetButton, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(StstusLabel))
                .addGap(18, 18, 18)
                .addComponent(generateRecordsButton)
                .addGap(18, 18, 18)
                .addComponent(endLabel)
                .addContainerGap(39, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ConncetButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConncetButtonActionPerformed
        try {
            String userName = UserFiled.getText();
            char[] password = PasswordField.getPassword();
            String SID = SIDField.getText();
            String port = PortField.getText();
            String hostName = HostFiled.getText();
            if (connection.getConnection(userName, hostName, SID, port, password)) {
                setSuccesStatusLabel();
                generateRecordsButton.setVisible(true);
                System.out.println(" count Users: " + connection.countUsers());
                System.out.println(" count Orders: " + connection.countOrders());
                System.out.println(" count Producer: " + connection.countProducer());
            } else {
                generateRecordsButton.setVisible(false);
                setFaultStatusLabel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }//GEN-LAST:event_ConncetButtonActionPerformed

    private void generateRecordsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateRecordsButtonActionPerformed
        insertUser(10);
        insertProducer(10);
        insertProduct(10);
        insertOrder(10);
        insertAdress(10);
        insertDelivery(10);
        insertComplaint(10);
        insertPayments(10);
        insertReturn(10);
        endLabel.setVisible(true);
    }//GEN-LAST:event_generateRecordsButtonActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try {
            connection.closeConnection();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    public void insertUser(int countUsers) {
        System.out.println("Statr generate users");
        for (int i = 0; i < countUsers; i++) {
            User user = new User();
            connection.insertUser(user);
        }
    }

    public void insertProducer(int countProducer) {
        System.out.println("Statr generate producers");
        for (int i = 0; i < countProducer; i++) {
            Producer producer = new Producer();
            connection.insertProducer(producer);
        }
    }

    private int randomProducerId(int countRecords) {
        Random random = new Random();
        int id = random.nextInt(countRecords) + 1;
        if (connection.checkProducerById(id) == 1) {
            return id;
        } else {
            return id = random.nextInt(5) + 1;
        }
    }
    private int randomDeliveryId(int countRecords) {
        Random random = new Random();
        int id = random.nextInt(countRecords) + 1;
        if (connection.checkDeliveryById(id) == 1) {
            return id;
        } else {
            return id = random.nextInt(5) + 1;
        }
    }
    private int randomAdressId(int countRecords) {
        Random random = new Random();
        int id = random.nextInt(countRecords) + 1;
        if (connection.checkAdressById(id) == 1) {
            return id;
        } else {
            return id = random.nextInt(5) + 1;
        }
    }
    private int randomProductId(int countRecords) {
        Random random = new Random();
        int id = random.nextInt(countRecords) + 1;
        if (connection.checkProductById(id) == 1) {
            return id;
        } else {
            return id = random.nextInt(5) + 1;
        }
    }
    private int randomUserId(int countRecords) {
        Random random = new Random();
        int id = random.nextInt(countRecords) + 1;
        if (connection.checkUserById(id) == 1) {
            return id;
        } else {
            return id = random.nextInt(5) + 1;
        }
    }

    public void insertProduct(int countProduct) {
        System.out.println("Statr generate products");
        for (int i = 0; i < countProduct; i++) {
            Product product = new Product(randomProducerId(connection.countProducer()));
            System.out.println(product.toString());
            connection.insertProduct(product);
        }
    }

    
    
    public void insertOrder(int countOrder) {
        System.out.println("Statr generate orderds");
        for (int i = 0; i < countOrder; i++) {
            Order order = new Order(randomUserId(connection.countUsers()),randomProductId(connection.countProduct()));
            connection.insertOrder(order);
        }
    }

    public void insertAdress(int countAdress) {
        System.out.println("Statr generate adress");
        for (int i = 0; i < countAdress; i++) {
            Adress adress = new Adress();
            connection.insertAdress(adress);
        }
    }

    public void insertDelivery(int countDelivery) {
        System.out.println("Statr generate delivery");
        for (int i = 0; i < countDelivery; i++) {
            Delivery delivery = new Delivery(randomAdressId(connection.countAdress()));
            connection.insertDelivery(delivery);
        }
    }

    public void insertComplaint(int countComplaint) {
        System.out.println("Statr generate complaint");
        for (int i = 0; i < countComplaint; i++) {
            Complaint complaint = new Complaint(randomUserId(connection.countUsers()));
            connection.insertComplaint(complaint);
        }
    }

    public void insertPayments(int countPayments) {
        System.out.println("Statr generate payments");
        for (int i = 0; i < countPayments; i++) {
            Payment payment = new Payment();
            connection.insertPayment(payment);
        }
    }

    public void insertReturn(int countReturn) {
        System.out.println("Statr generate return");
        for (int i = 0; i < countReturn; i++) {
            Return returnn = new Return(randomDeliveryId(connection.countDelivery()),randomAdressId(connection.countAdress()));
            connection.insertReturn(returnn);
        }
    }

    public void setSuccesStatusLabel() {
        StstusLabel.setText("Połączono z bazą danych!");
        StstusLabel.setForeground(Color.green);
    }

    public void setFaultStatusLabel() {
        StstusLabel.setText("Brak połączenia z bazą danych!");
        StstusLabel.setForeground(Color.red);
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ConncetButton;
    private javax.swing.JTextField HostFiled;
    private javax.swing.JLabel HostLabel;
    private javax.swing.JPasswordField PasswordField;
    private javax.swing.JLabel PasswordLabel;
    private javax.swing.JTextField PortField;
    private javax.swing.JLabel PortLabel;
    private javax.swing.JTextField SIDField;
    private javax.swing.JLabel SIDLabel;
    private javax.swing.JLabel StstusLabel;
    private javax.swing.JTextField UserFiled;
    private javax.swing.JLabel UserLabel;
    private javax.swing.JLabel endLabel;
    private javax.swing.JButton generateRecordsButton;
    // End of variables declaration//GEN-END:variables
}
