package generatorrekordow;

import generatorrekordow.models.*;
import java.sql.*;

public class OracleConnection {

    Connection connection = null;
    boolean conncetionStatus = false;

    public boolean getConnection(String username, String hostName, String SID, String port, char[] pass) throws SQLException {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            String password = new String(pass);
            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@" + hostName + ":" + port + ":" + SID, username, password);
            System.out.println("Succes");
            return conncetionStatus = true;
        } catch (Exception e) {
            System.out.print(e);
            return conncetionStatus = false;
        }
    }

    public void closeConnection() throws SQLException {
        if (conncetionStatus) {
            connection.close();
            System.out.println("connection is closed");
        }
    }

    public void insertUser(User user) {
        String us = "\"User\"";
        String insertQuery = "INSERT INTO " + us + "(NAME, SURNAME,MAIL,PHONE) VALUES" + "('" + user.getName() + "','" + user.getSurname() + "','" + user.getMail() + "','" + user.getPhone() + "')";
        try {
            Statement statement = connection.createStatement();
            System.out.println(insertQuery);
            statement.executeUpdate(insertQuery);
            statement.close();
        } catch (Exception e) {
            e.toString();
        }
    }

    public void insertProducer(Producer producer) {
        String insertQuery = "INSERT INTO Producer (NAME, WEBSITE,MAIL,PHONE) VALUES" + "('" + producer.getName() + "','" + producer.getWebsite() + "','" + producer.getMail() + "','" + producer.getPhone() + "')";
        try {
            Statement statement = connection.createStatement();
            System.out.println(insertQuery);
            statement.executeUpdate(insertQuery);
            statement.close();
        } catch (Exception e) {
            e.toString();
        }
    }

    public void insertProduct(Product product) {
        String insertQuery = "INSERT INTO Product (NAME,PRICE,PRODUCER_ID) VALUES" + "('" + product.getName() + "','" + product.getPrice() + "','" + product.getProducerId() + "')";
        System.out.println(insertQuery);
        try {
            Statement statement = connection.createStatement();
            System.out.println(insertQuery);
            statement.executeUpdate(insertQuery);
            statement.close();
        } catch (Exception e) {
            e.toString();
        }
    }

    public void insertOrder(Order order) {
        String orderName = "\"Order\"";
        String insertQuery = "INSERT INTO " + orderName + "(USER_ID,PRODUCT_ID,STATUS) VALUES" + "('" + order.getUserId() + "','" + order.getProductId() + "','" + order.getStatusId() + "')";
        try {
            Statement statement = connection.createStatement();
            System.out.println(insertQuery);
            statement.executeUpdate(insertQuery);
            statement.close();
        } catch (Exception e) {
            e.toString();
        }
    }
    public void insertAdress(Adress adress) {
        String adressName = "\"Adress\"";
        String insertQuery = "INSERT INTO " + adressName + "(CITY,STREET,HOUSENUMBER) VALUES" + "('" + adress.getCity() + "','" + adress.getStreet() + "','" + adress.getNumber() + "')";
        try {
            Statement statement = connection.createStatement();
            System.out.println(insertQuery);
            statement.executeUpdate(insertQuery);
            statement.close();
        } catch (Exception e) {
            e.toString();
        }
    }
    public void insertDelivery(Delivery delivery) {
        String insertQuery = "INSERT INTO Delivery (DELIVERYSTATUS,DELIVERYADDRESS) VALUES" + "('" + delivery.getDeliveryStatus() + "','" + delivery.getDeliveryAdress() + "')";
        try {
            Statement statement = connection.createStatement();
            System.out.println(insertQuery);
            statement.executeUpdate(insertQuery);
            statement.close();
        } catch (Exception e) {
            e.toString();
        }
    }
    public void insertComplaint(Complaint complaint) {
        String insertQuery = "INSERT INTO Complaint (DESCRIPTION,STATUS,User_Id) VALUES" + "('" + complaint.getDescription() + "','" + complaint.getStatus() + "','" + complaint.getUserId() + "')";
        try {
            Statement statement = connection.createStatement();
            System.out.println(insertQuery);
            statement.executeUpdate(insertQuery);
            statement.close();
        } catch (Exception e) {
            e.toString();
        }
    }
    public void insertPayment(Payment payment) {
        String insertQuery = "INSERT INTO Payment (CURRENCY,STATUS,PRICE) VALUES" + "('" + payment.getCurrency() + "','" + payment.getStatus() + "','" + payment.getPrice() + "')";
        try {
            Statement statement = connection.createStatement();
            System.out.println(insertQuery);
            statement.executeUpdate(insertQuery);
            statement.close();
        } catch (Exception e) {
            e.toString();
        }
    }
    public void insertReturn(Return returnn) {
        String returnName = "\"Return\"";
        String insertQuery = "INSERT INTO " + returnName + " (DELIVERY_ID,STATUS,ADRESS_ID) VALUES" + "('" + returnn.getDelivery() + "','" + returnn.getStatus() + "','" + returnn.getAdress() + "')";
        try {
            Statement statement = connection.createStatement();
            System.out.println(insertQuery);
            statement.executeUpdate(insertQuery);
            statement.close();
        } catch (Exception e) {
            e.toString();
        }
    }
    public int countUsers() {
        String countQuery = "SELECT COUNT(*) AS rowcount FROM \"User\"";
        int countUsers = 0;
        try {
            Statement statement = connection.createStatement();
            System.out.print(countQuery);
            ResultSet resultSet = statement.executeQuery(countQuery);
            resultSet.next();
            countUsers = resultSet.getInt("rowcount");
            resultSet.close();
            statement.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return countUsers;
    }
    public int countOrders() {
        String countQuery = "SELECT COUNT(*) AS rowcount FROM \"Order\"";
        int countOrders = 0;
        try {
            Statement statement = connection.createStatement();
            System.out.print(countQuery);
            ResultSet resultSet = statement.executeQuery(countQuery);
            resultSet.next();
            countOrders = resultSet.getInt("rowcount");
            resultSet.close();
            statement.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return countOrders;
    }
    public int countProducer() {
        String countQuery = "SELECT COUNT(*) AS rowcount FROM Producer";
        int countProducers = 0;
        try {
            Statement statement = connection.createStatement();
            System.out.print(countQuery);
            ResultSet resultSet = statement.executeQuery(countQuery);
            resultSet.next();
            countProducers = resultSet.getInt("rowcount");
            resultSet.close();
            statement.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return countProducers;
    }
    public int countAdress() {
        String countQuery = "SELECT COUNT(*) AS rowcount FROM \"Adress\"";
        int countAdress = 0;
        try {
            Statement statement = connection.createStatement();
            System.out.print(countQuery);
            ResultSet resultSet = statement.executeQuery(countQuery);
            resultSet.next();
            countAdress = resultSet.getInt("rowcount");
            resultSet.close();
            statement.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return countAdress;
    }
    public int countProduct() {
        String countQuery = "SELECT COUNT(*) AS rowcount FROM Product";
        int countProduct = 0;
        try {
            Statement statement = connection.createStatement();
            System.out.print(countQuery);
            ResultSet resultSet = statement.executeQuery(countQuery);
            resultSet.next();
            countProduct = resultSet.getInt("rowcount");
            resultSet.close();
            statement.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return countProduct;
    }
    public int countDelivery() {
        String countQuery = "SELECT COUNT(*) AS rowcount FROM Delivery";
        int countDelivery = 0;
        try {
            Statement statement = connection.createStatement();
            System.out.print(countQuery);
            ResultSet resultSet = statement.executeQuery(countQuery);
            resultSet.next();
            countDelivery = resultSet.getInt("rowcount");
            resultSet.close();
            statement.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return countDelivery;
    }
    public int checkProducerById(int id) {
        String countQuery = "SELECT COUNT(*) AS rowcount FROM Producer where ProducerId=" + id;
        int countProducers = 0;
        try {
            Statement statement = connection.createStatement();
            System.out.print(countQuery);
            ResultSet resultSet = statement.executeQuery(countQuery);
            resultSet.next();
            countProducers = resultSet.getInt("rowcount");
            resultSet.close();
            statement.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return countProducers;
    }
    public int checkProductById(int id) {
        String countQuery = "SELECT COUNT(*) AS rowcount FROM Product where ProductId=" + id;
        int countProduct = 0;
        try {
            Statement statement = connection.createStatement();
            System.out.print(countQuery);
            ResultSet resultSet = statement.executeQuery(countQuery);
            resultSet.next();
            countProduct = resultSet.getInt("rowcount");
            resultSet.close();
            statement.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return countProduct;
    }
    public int checkUserById(int id) {
        String countQuery = "SELECT COUNT(*) AS rowcount FROM \"User\" where UserId=" + id;
        int countUsers = 0;
        try {
            Statement statement = connection.createStatement();
            System.out.print(countQuery);
            ResultSet resultSet = statement.executeQuery(countQuery);
            resultSet.next();
            countUsers = resultSet.getInt("rowcount");
            resultSet.close();
            statement.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return countUsers;
    }
    public int checkAdressById(int id) {
        String countQuery = "SELECT COUNT(*) AS rowcount FROM \"Adress\" where AdressId=" + id;
        int countAdress = 0;
        try {
            Statement statement = connection.createStatement();
            System.out.print(countQuery);
            ResultSet resultSet = statement.executeQuery(countQuery);
            resultSet.next();
            countAdress = resultSet.getInt("rowcount");
            resultSet.close();
            statement.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return countAdress;
    }
    public int checkDeliveryById(int id) {
        String countQuery = "SELECT COUNT(*) AS rowcount FROM Delivery where DeliveryId=" + id;
        int countDelivery = 0;
        try {
            Statement statement = connection.createStatement();
            System.out.print(countQuery);
            ResultSet resultSet = statement.executeQuery(countQuery);
            resultSet.next();
            countDelivery = resultSet.getInt("rowcount");
            resultSet.close();
            statement.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return countDelivery;
    }
}
